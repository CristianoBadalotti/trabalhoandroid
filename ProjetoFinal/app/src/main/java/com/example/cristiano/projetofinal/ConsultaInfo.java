package com.example.cristiano.projetofinal;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Cristiano on 25/02/2016.
 */
public class ConsultaInfo extends AsyncTask<Void, Void, String> {

    private ConsultaListener listener;

    private static final String URL_BEGIN = "http://api.openweathermap.org/data/2.5/weather?q=";
    private static final String URL_END = ",pt&lang=pt&appid=44db6a862fba0b067b1930da0d769e98";

    private String city = null;

    public ConsultaInfo(ConsultaListener listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(Void... params) {
        try {
            String resultado = consultarServidor();

            return criaResultado(resultado);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setCity(String city) {
        this.city = city;
    }

    private String criaResultado(String s) throws JSONException{
        JSONObject object = new JSONObject(s);

        JSONArray array = object.getJSONArray("weather");
        JSONObject jsonObjectWeather = array.getJSONObject(0);

        int id = jsonObjectWeather.getInt("id");
        String descricao = jsonObjectWeather.getString("description");
        String icon = jsonObjectWeather.getString("icon");

        JSONObject jo = object.getJSONObject("sys");
        String country = jo.getString("country");

        return "Situação: "+ descricao;
    }

    private String consultarServidor() throws IOException{
        InputStream is = null;
        try {
            String fullURL = URL_BEGIN + city + URL_END;
            URL url = new URL(fullURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            conn.getResponseCode();

            is = conn.getInputStream();

            Reader reader = null;
            reader = new InputStreamReader(is);
            char[] buffer = new char[2048];
            reader.read(buffer);
            return new String(buffer);

        }finally {
            if (is != null) {
                is.close();
            }
        }
    }

    @Override
    protected void onPostExecute(String s) {
        listener.onConsultaConcluida(s);
    }

    public interface ConsultaListener{
        void onConsultaConcluida(String s);
    }
}
