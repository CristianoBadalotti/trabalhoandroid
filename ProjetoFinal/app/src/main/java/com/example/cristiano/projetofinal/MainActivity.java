package com.example.cristiano.projetofinal;

import com.example.cristiano.projetofinal.ConsultaInfo.ConsultaListener;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Button;

import android.os.Handler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    private Handler handler = new Handler();

    private String stringJSON = "";

    private static final String URL_BEGIN = "http://api.openweathermap.org/data/2.5/weather?q=";
    private static final String URL_END = ",pt&lang=pt&appid=44db6a862fba0b067b1930da0d769e98";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onClickButton();
            }
        });
    }

    public void  onClickButton() {
        String str = ((SearchView) findViewById(R.id.searchView)).getQuery().toString();
        loadInfo(str);
        try {
            criaValores();
        } catch (JSONException e) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setStringJSON(String stringJSON) {
        this.stringJSON = stringJSON;
    }

    public void setDescricao(String s) {
        TextView tv = (TextView) findViewById(R.id.textView);
        tv.setText(s);
    }

    public void setLocal(String s) {
        TextView tv = (TextView) findViewById(R.id.textView2);
        tv.setText(s);
    }

    public void loadImg(final String nameIcon) {
        new Thread() {
            public void run() {
                Bitmap img = null;
                try {
                    URL urlImg = new URL("http://openweathermap.org/img/w/" + nameIcon + ".png");
                    HttpURLConnection conn = (HttpURLConnection) urlImg.openConnection();
                    InputStream input = conn.getInputStream();
                    img = BitmapFactory.decodeStream(input);
                } catch (IOException e) {

                }

                final Bitmap imgAux = img;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        ImageView iv = (ImageView) findViewById(R.id.imageView);
                        iv.setImageBitmap(imgAux);
                    }
                });
            }
        }.start();
    }

    public void loadInfo(String city) {
        final String cityAux = city;
        new Thread() {
            public void run() {
                InputStream is = null;
                String str = null;
                String fullURL = URL_BEGIN + cityAux + URL_END;
                try {
                    URL url = new URL(fullURL);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();
                    conn.getResponseCode();
                    is = conn.getInputStream();

                    Reader reader = null;
                    reader = new InputStreamReader(is);
                    char[] buffer = new char[2048];
                    reader.read(buffer);
                    str = new String(buffer);
                } catch (IOException e) {

                }

                final String strAux = str;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        setStringJSON(strAux);
                    }
                });
            }
        }.start();
    }

    public void criaValores() throws JSONException {
        JSONObject object = new JSONObject(stringJSON);

        JSONArray array = object.getJSONArray("weather");
        JSONObject jsonObjectWeather = array.getJSONObject(0);
        String descricao = jsonObjectWeather.getString("description");
        String icon = jsonObjectWeather.getString("icon");

        String nameCity = object.getString("name");

        JSONObject sysObject = object.getJSONObject("sys");
        String country = sysObject.getString("country");

        JSONObject mainObject = object.getJSONObject("main");
        double temp = mainObject.getDouble("temp") - 273;
        double tempMin = mainObject.getDouble("temp_min") - 273;
        double tempMax = mainObject.getDouble("temp_max") - 273;

        JSONObject windObject = object.getJSONObject("wind");
        int speed = (int)windObject.getDouble("speed");

        setDescricao((int)temp + "ºC - " +descricao + "\nMin: " + (int)tempMin + "ºC    Max: " + (int)tempMax +  "ºC");
        loadImg(icon);
        setLocal(nameCity + ", " + country);
    }
}
